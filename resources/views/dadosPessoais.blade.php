@extends('layouts.app')

@section('content')

{{$errors}}
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dados Pessoais de {{Auth::user()->name}} </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('dadosPessoais.store') }}">
                         @csrf
                    <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" name="name" id="inputGroup-sizing-default">Nome Completo: </span>
                          </div>
                          <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" placeholder="{{Auth::user()->name}}" disabled >
                      </div>

                        <div class="input-group mb-3">
                      <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroup-sizing-default">Data de Nascimento: </span>
                    </div>
                      <input id="data" type="date" class="form-control" name="data"></input>
                    </div>

                    <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text"  for="inputGroupSelect01">Sexo: </label>
                    </div>
                    <select name="sexo" class="custom-select" id="inputGroupSelect01" type="text">
                        <option selected disabled="">Escolher</option>
                        <option value="Masculino">Masculino</option>
                        <option value="Feminino">Feminino</option>
                    </select>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-default">Morada: </span>
                        </div>
                        <input name="morada" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>
                </div>
            </div>
                        <br>
                        <br>
                        
                        <div class="col-md-6 offset-md-4">   
                            <button type="reset" class="btn btn-info"> Limpar </button>
                            <button type="submit" class="btn btn-info">
                                    {{ __('Registar') }}
                                    </button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
