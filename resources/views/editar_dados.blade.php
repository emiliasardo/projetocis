@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div>
                        @if(Auth::user()->tipo =='utente')
                        <form method="get" action="{{ route('dadosPessoais.create') }}">
                            <button type="submit" class="btn btn-info">Dados Pessoais</button>
                        </form>

                        <br>

                        <form method="get" action="{{ route('dadosClinicos.create') }}">
                            <button type="submit" class="btn btn-info">Dados Clínicos</a> </button>
                        </form>
                        @endif

                        @if(Auth::user()->tipo =='prof')
                        <form method="get" action="{{ route('dadosPessoais.create') }}">
                            <button type="submit" class="btn btn-info">Dados Pessoais</button>
                        </form>
                        @endif
        
                        @if(Auth::user()->tipo =='admin')
                        <form method="get" action="http://homestead.projeto2/dadosPessoais">
                            <button type="submit" class="btn btn-info">Dados Pessoais</button>
                        </form>
                        @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
