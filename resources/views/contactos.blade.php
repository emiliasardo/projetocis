@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1 align ="left"> Contactos</h1><br>
            <div class="links" align="left">
              <h5>Carlos Bruno</h5><br>
              <b>Email:</b><br>
              carlos@lisboa.pt<br>
              <br>
              <b>Telefone:</b> (+351) 111 111 111
              <br>
              <b>Telemóvel:</b> (+351) 222 222 222
              <br><br>
              <h5>João Pedro</h5><br>
              <b>Email:</b><br>
              joao@porto.pt<br>
              <br>
              <b>Telefone:</b> (+351) 333 333 333
              <br>
              <b>Telemóvel:</b> (+351) 444 444 444
            </div>
        </div>
    </div>
</div>
@endsection
