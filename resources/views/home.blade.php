@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div style="font-weight: bold;" class="card-header">Bem Vindo!</div>

				<br>
				<div align="center">
					<table cellpadding="10">
	                    <tr>
	                        <h5 style="font-weight: bold;">Novo Sistema de Controlo da Glicose</h5>
	                    </tr>

	                    <br>

	                    <tr>    
	                        <iframe width="560" height="315" src="https://www.youtube.com/embed/lM8aDzDZhFY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
	                    </tr>
						
						<br>
						<br>
						<hr size="10" width="50%" align="center" noshade>
						<br>

						<tr>
	                        <h5 style="font-weight: bold;">Associação de Jovens Diabéticos de Portugal AJDP</h5>
	                    </tr>

	                    <br>

	                    <tr>    
	                        <iframe width="560" height="315" src="https://www.youtube.com/embed/HF0AAhTFVH4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
	                    </tr>

	                </table>
	            </div>
            </div>
        </div>
    </div>
</div>
@endsection
