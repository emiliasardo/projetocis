@extends('layouts.app')

@section('content')
<style>
.card-body{
    text-align: right;
}
.card-body h3{
    text-align: left;
}
.card-body h6{
    text-align: left;
}
.card-body label{
    text-align: left;
}
</style>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Perfil de {{Auth::user()->name}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                  <h3>  Dados Pessoais </h3>
                      <br>

                      <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default">Nome Completo: </span>
                          </div>
                          <input type="text" placeholder="{{Auth::user()->name}}" disabled class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                      </div>

                        <div class="input-group mb-3">
                      <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroup-sizing-default">Data de Nascimento: </span>
                    </div>
                      <input id="data" type="date" class="form-control" name="data"></input>
                    </div>

                    <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="inputGroupSelect01">Sexo: </label>
                    </div>
                    <select class="custom-select" id="inputGroupSelect01">
                        <option selected>Escolher</option>
                        <option value="Masculino">Masculino</option>
                        <option value="Feminino">Feminino</option>
                    </select>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-default">Morada: </span>
                        </div>
                        <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                  <h3>  Dados Clínicos </h3>
                      <br>

                        <div class="input-group mb-3">
                      <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroup-sizing-default">Inicio da diabetes: </span>
                    </div>
                      <input id="data" type="date" class="form-control" name="data"></input>
                    </div>
                    <div class="input-group mb-3">
                    <div class="form-group">
                    <h6><label for="comment">Doenças associadas:</label></h6>
                    <textarea class="form-control" rows="5" cols="100" id="doencas" name="doencas"></textarea>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-default">Peso (Kg): </span>
                        </div>
                        <input type="text" class="form-control" name="peso" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-default">Altura (cm): </span>
                        </div>
                        <input type="text" name="altura" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-default">IMC: </span>
                        </div>
                        <input type="text" name="imc" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>

                    <div class="input-group mb-3">
                    <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Registo Diário: </span>
                    </div>
                    <input id="datetime" type="datetime-local" class="form-control" name="registo"></input>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-default">Glicémia: </span>
                        </div>
                        <input type="text" name="glicemia" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-default">Insulina (unidades): </span>
                        </div>
                        <input type="text" name="insulina" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>
                            <div> 
                                <form method="get" action="http://homestead.projeto2/editarDados">
                                    <button type="submit" class="btn btn-info"> Editar Dados</button>
                                </form>        
                                
                            </div>
    
                    
  
@endsection
