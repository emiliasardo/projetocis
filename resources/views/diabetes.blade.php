@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1 align ="left"> Diabetes Mellitus</h1><br>
            <div class="links" align="left">
              <p>Segundo a Sociedade Portuguesa de Diabetologia (2015), “diabetes mellitus descreve uma <br>
                 desordem metabólica de etiologia múltipla, caracterizada por uma hiperglicemia crónica com distúrbios no <br>
                  metabolismo resultantes de deficiências na secreção ou ação da insulina, ou ambas.”.<br>
Segundo a mesma fonte, os efeitos a longo prazo da DM podem provocar desde cegueira, insuficiência renal, <br>
ulcerações nos pés levando à sua amputação e disfunção sexual (Sociedade Portuguesa de Diabetologia, 2015).<br>
Existem alguns tipos de DM, sendo que os mais recorrentes são:
</p>
<ul>
<li>Diabetes Mellitus Tipo 1, que se desenvolve mais frequentemente em crianças, jovens e jovens adultos. <br> Neste tipo, é o pâncreas que deixa de produzir insulina e a única maneira de o controlar é administrando a mesma.</li>
<p>
<li>Diabetes Mellitus Tipo 2, surge em qualquer idade e é mais frequente em pessoas com obesidade. <br> Neste caso, o corpo vai produzindo menos quantidade de insulina, criando uma resistência à mesma, <br> e é tratado com uma mudança de estilo de vida, com medicamentos e, mais tarde, com a administração de insulina.</li>
<p>
<li>Existem outros tipos de DM como a Diabetes Gestacional, ou causado por medicamentos, <br> traumatismos graves, formas genéticas, entre outros.</li>
</ul>
(Controlar a Diabetes, 2017).
<p>
	<div class="links" align="center">
		<img src="https://pplware.sapo.pt/wp-content/uploads/2018/01/diabetes_capa-720x480.jpg" style="width:540px;height:280px;">
		<p><h6> Fonte: <i>https://pplware.sapo.pt/wp-content/uploads/2018/01/diabetes_capa-720x480.jpg.</h6></i></p>
	</div>
            </div>
        </div>
    </div>
</div>
@endsection
