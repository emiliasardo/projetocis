@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
               
                <div class="card-header" align="center">
                    <h2>Profissional de Saúde </h2>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('profissional.store') }}">
                    @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right"> Nome Completo</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" autofocus>

                                @if ($errors->has('nome'))
                                    <span class="invalid-feedback">
                                     <strong>{{ $errors->first('nome') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" >

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                      <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                 @endif
                            </div>
                        </div>
                   
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirmar Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
                            </div>
                        </div>

                        <br>
                        <br>

                        <div class="form-group row">
                            <label for="text" class="col-md-4 col-form-label text-md-right">Número de Cédula Profissional</label>
                             
                            <div class="col-md-6">
                                <input id="cedula" type="text" class="form-control{{ $errors->has('cedula') ? ' is-invalid' : '' }}" name="cedula" value="{{ old('cedula') }}" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="text" class="col-md-4 col-form-label text-md-right">Área de Atuação </label>
                         
                            <div class="col-md-6">
                                <select id="area" class="form-control{{ $errors->has('area') ? ' is-invalid' : '' }}" name="area" value="{{ old('area') }}" style="width: 330px; height: 35px">
                                    <option selected disabled>Selecione a sua área</option>
                                    <option value="Enfermagem">Enfermagem</option>
                                    <option value="Dietética">Dietética</option>
                                    <option value="Nutrição">Nutrição</option>
                                    <option value="Fisioterapia">Fisioterapia</option>
                                    <option value="Terapia Ocupacional">Terapia Ocupacional</option>
                                    <option value="Outra">Outra</option> 
                                </select>
                            </div>
                        </div>

                        <div class="form-group row" hidden id="indarea">
                            <label for="text" class="col-md-4 col-form-label text-md-right">Indique a sua área</label>
                         
                            <div class="col-md-6" >
                                <textarea  class="form-control" name="indarea"></textarea>
                            </div>
                        </div>

                        <br>

                        <table align="right" cellpadding="10">
                            <tr>
                                <td>
                                    <button type="submit" class="btn btn-info">
                                    {{ __('Registar') }}
                                    </button>
                                </td>

                                <td>
                                    <form method="get" action="http://homestead.projeto2/selecao-registo">
                                        <button type="submit" class="btn btn-info"> Voltar</button>
                                    </form> 
                                </td>
                            </tr>
                        </table>
                    </form>                        
                </div> 
            </div>
        </div>
    </div>
</div>
@endsection