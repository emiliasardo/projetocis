<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
      <!--Editado por Ruben Abreu-->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Care4Diabetes</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            form {
               display: flex;
               flex-direction: column;
               align-items: right;
               background-color: #8DC7D3;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <form>
                    <div class="top-right links">
                        @auth
                            <a href="{{ url('/home') }}">Página Inicial</a>
                        @else
                            <a href="{{ route('login') }}">Login</a>
                            <a href="{{ route('registo-novo') }}">Registo</a>
                        @endauth
                    </div>
                </form>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Care4Diabetes
                </div>
           <div class="links" align="center">
           <img src="https://pplware.sapo.pt/wp-content/uploads/2018/01/diabetes_capa-720x480.jpg" style="width:540px;height:280px;">
        </div>
    </div>
    </body>
</html>