@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1 align ="left"> Links Úteis </h1>
            <br>
            <h6 align ="left"> Aqui encontram-se alguns sites importantes para o informar. </h6>
                <br>
            <div class="links" align="left">
                <h5> <a href="https://www.apdp.pt/diabetes/a-pessoa-com-diabetes/o-que-e-a-diabetes">APDP - Portal dos Diabetes</a> </h5> <br>
                <h5> <a href="http://www.mydiabetes.pt/">MyDiabetes</a> </h5> <br>
                <h5> <a href="http://www.medinfar.pt/2014/04/o-que-e-a-diabetes/">Grupo MEDINFAR</a> </h5> <br>
                </div>
        </div>
    </div>
</div>
@endsection
