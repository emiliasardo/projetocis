@extends('layouts.app')

@section('content')
{{$errors}}
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dados Clínicos</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('dadosClinicos.store') }}">
                        @csrf
                    <div align="left">
                   
                        <div class="input-group mb-3">
                      <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroup-sizing-default">Inicio da diabetes: </span>
                    </div>
                      <input id="data" type="date" class="form-control" name="data"></input>
                    </div>
                    <div class="input-group mb-3">
                    <div class="form-group">
                     <span class="input-group-text" id="inputGroup-sizing-default">Doenças Associadas: </span>
                    <textarea class="form-control" rows="5" cols="100" id="doencas" name="doencas" type="text"></textarea>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-default">Peso (Kg): </span>
                        </div>
                        <input id="peso" type="integer" class="form-control" name="peso" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-default">Altura (cm): </span>
                        </div>
                        <input id="altura" name="altura" type="integer" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-default">IMC: </span>
                        </div>
                        <input id="imc" name="imc" type="integer" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>

                    <div class="input-group mb-3">
                    <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Registo Diário: </span>
                    </div>
                    <input id="registo" type="datetime-local" class="form-control" name="registo"></input>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-default">Glicémia: </span>
                        </div>
                        <input id="glicemia" type="integer" class="form-control" name="glicemia" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-default">Insulina (unidades): </span>
                        </div>
                        <input id="insulina" name="insulina" type="integer" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>
                            </div>       

                        <div class="col-md-6 offset-md-4">

                            <button type="reset" class="btn btn-info"> Limpar </button>
                            <button type="submit" class="btn btn-info"> {{ __('Registar') }} </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
