<?php

return [
    'success' => [
        'title'  => 'Muito bem!',
        'reason' => [
            'submitted_to_post'       => 'Resposta enviada com sucesso para '.mb_strtolower(trans('chatter::intro.titles.discussion')).'.',
            'updated_post'            => 'Atualizou com sucesso o '.mb_strtolower(trans('chatter::intro.titles.discussion')).'.',
            'destroy_post'            => 'Eliminou com sucesso a resposta e '.mb_strtolower(trans('chatter::intro.titles.discussion')).'.',
            'destroy_from_discussion' => 'Eliminou com sucesso a resposta do '.mb_strtolower(trans('chatter::intro.titles.discussion')).'.',
            'created_discussion'      => 'Criado com sucesso um novo '.mb_strtolower(trans('chatter::intro.titles.discussion')).'.',
        ],
    ],
    'info' => [
        'title' => 'Atenção!',
    ],
    'warning' => [
        'title' => 'Wuh Oh!',
    ],
    'danger'  => [
        'title'  => 'Oh Snap!',
        'reason' => [
            'errors'            => 'Por favor corrija os seguintes erros:',
            'prevent_spam'      => 'Para evitar spam, aguarde pelo menos: 1 minuto entre o envio do conteúdo.',
            'trouble'           => 'Desculpe, parece ter havido um problema ao enviar sua resposta.',
            'update_post'       => 'Não foi possível atualizar a sua resposta. Certifique-se de que não está a fazendo nada errado.',
            'destroy_post'      => 'Não foi possível apagar a sua resposta. Certifique-se de que não está a fazendo nada errado.',
            'create_discussion' => 'Parece haver um problema ao criar sua'.mb_strtolower(trans('chatter::intro.titles.discussion')).'.',
        	'title_required'    => 'Por favor, escreva um título',
        	'title_min'		    => 'O título tem que ter pelo menos: min caracteres.',
        	'title_max'		    => 'O título não pode ter mais que: max caracteres.
',
        	'content_required'  => 'Por favor, escreva algum conteúdo',
        	'content_min'  		=> 'O conteúdo deve ter pelo menos: min caracteres
',
        	'category_required' => 'por favor escolha uma categoria',

       
       
        ],
    ],
];
