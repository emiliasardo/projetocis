<?php

return [
    'words' => [
        'cancel'  => 'Cancelar',
        'delete'  => 'Apagar',
        'edit'    => 'Editar',
        'yes'     => 'Sim',
        'no'      => 'Não',
        'minutes' => '1 minuto| :count minutes',
    ],

    'discussion' => [
        'new'          => 'Nova '.trans('chatter::intro.titles.discussion'),
        'all'          => 'Todas as '.trans('chatter::intro.titles.discussions'),
        'create'       => 'Criar '.trans('chatter::intro.titles.discussion'),
        'posted_by'    => 'Inserido por',
        'head_details' => 'Inserido na Categoria',

    ],
    'response' => [
        'confirm'     => 'Tem a certeza que deseja eliminar esta resposta?',
        'yes_confirm' => 'Sim Eliminar',
        'no_confirm'  => 'Não, obrigado',
        'submit'      => 'Enviar resposta',
        'update'      => 'Atualizar resposta',
    ],

    'editor' => [
        'title'               => 'Título de '.trans('chatter::intro.titles.discussion'),
        'select'              => 'Selecione uma categoria',
        'tinymce_placeholder' => 'Insira o seu texto aqui...',
        'select_color_text'   => 'Selecione uma cor '.trans('chatter::intro.titles.discussion').' (optional)',
    ],

    'email' => [
        'notify' => 'Notifique-me quando alguém responder',
    ],

    'auth' => 'Por favor, faça <a href="/:home/login">login</a>
                ou <a href="/:home/register">registe-se</a>
                para deixar uma resposta.',

];
