<?php

return [
    'preheader'       => 'Recebeu uma resposta à sua publicação no fórum.',
    'greeting'        => 'Olá,',
    'body'            => 'Recebeu uma resposta à sua publicação no fórum em',
    'view_discussion' => 'View the '.mb_strtolower(trans('chatter::intro.titles.discussion')).'.',
    'farewell'        => 'Tenha um ótimo dia!',
    'unsuscribe'      => [
        'message' => 'Se não quiser ser notificado quando alguém responder a esta publicação do formulário, retire a seleção da configuração de notificação na parte inferior da página',
        'action'  => 'Não gosta destes emails?',
        'link'    => 'Anular subscrição para '.mb_strtolower(trans('chatter::intro.titles.discussion')).'.',
    ],
];

