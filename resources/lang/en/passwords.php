<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A Password deve ter pelo menos 6 caracteres.',
    'reset' => 'A sua password foi atualizada!',
    'sent' => 'Foi enviado um e-mail para modificar a sua password!',
    'token' => 'Password inválida.',
    'user' => "Não foi encontrado nunhum utilizador com esse e-mail.",

];
