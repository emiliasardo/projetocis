<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDadosClinicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dados_clinicos', function (Blueprint $table) {
            $table->increments('id');
            $table->date('data');
            $table->string('doencas');
            $table->integer('peso');
            $table->integer('altura');
            $table->integer('imc');
            $table->date('registo');
            $table->integer('glicemia');
            $table->integer('insulina');
            $table->timestamps();

             $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dados_clinicos');
    }
}
