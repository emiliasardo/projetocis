<?php

use Illuminate\Database\Seeder;
use App\Event;

class AddDummyEvent extends Seeder
{
    public function run()
    {
        $data = [
          ['title'=>'Dia Mundial do Diabetes', 'start_date'=>'2018-11-14', 'end_date'=>'2018-11-14'],
          ['title'=>'Dia Mundial da Saúde', 'start_date'=>'2018-04-7', 'end_date'=>'2018-04-7'],
          ['title'=>'	13.ª Reunião do Núcleo de Estudos da Diabetes Mellitus', 'start_date'=>'2018-10-26', 'end_date'=>'2018-10-28'],
          ['title'=>'28th European Childhood Obesity Group Meeting - ECOG 2018', 'start_date'=>'2018-11-14', 'end_date'=>'2018-11-17']
        ];
        foreach ($data as $key => $value) {
        	Event::create($value);
        }
    }
}
