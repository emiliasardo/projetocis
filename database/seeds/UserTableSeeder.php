<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;

class UserTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
   

    $utente = new User();
    $utente->name = 'Utente';
    $utente->email = 'utente@utente.pt';
    $utente->password = bcrypt('utente');
    $utente->tipo = 'utente';
    $utente->save();

    $ps = new User();
    $ps->name = 'Profissional';
    $ps->email = 'profissional@profissional.pt';
    $ps->password = bcrypt('profissional');
    $ps->tipo = 'prof';
    $ps->save();

    $admin = new User();
    $admin->name = 'Admin';
    $admin->email = 'admin@admin.pt';
    $admin->password = bcrypt('admin');
    $admin->tipo = 'admin';
    $admin->save();
  }
}