<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Gerardojbaez\Messenger\Contracts\MessageableInterface;
use Gerardojbaez\Messenger\Traits\Messageable;

class User extends Authenticatable  implements MessageableInterface{
    use Notifiable;
    use Messageable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function utente()
    {
        return $this->hasOne('App\Utente');
    }

    public function profissional (){
        return $this->hasOne('App\profissional', 'id_user');
    }

        public function admin (){
        return $this->hasOne('App\admin', 'id_user');
    }

     public function dadosPessoais (){
        return $this->hasOne('App\DadosPessoais');
    }

     public function dadosClinicos (){
        return $this->hasOne('App\DadosClinicos');
    }
}
