<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Utente extends Model
{
    //
    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
