<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Utilizador;
use App\User;

class UtilizadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $utentes = Utilizador::all();
        
        return view('listaUtilizadores', compact('utilizadores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('novoUtilizador');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validacoes = [
      'nome' => 'required',
      'sexo' => 'required',
      'dataNasc' => 'required',
      'sns' => 'required|integer|min:9',
      'morada' => 'required',
      'peso' => 'required',
      'altura' => 'required',
      'name' => 'required',
      'email'=> 'required',
      'password'=> 'required',
    ];

    $mensagens = [
      'nome.required' => 'O nome é obrigatório!',
      'sexo.required' => 'O sexo é obrigatório!',
      'dataNasc.required' => 'A data de nascimento é obrigatória!',
      'sns.required'=>'O número de SNS é obrigatório!',
      'sns.integer'=>'O número de SNS tem de ser um número inteiro!',
      'sns.min'=>'O número de SNS tem de ter 9 algarismos!',
      'morada.required'=>'A morada é obrigatória!',
      'peso.required'=>'O peso é obrigatório!',
      'peso.min'=>'O peso tem de ter 2 algarismos!',
      'altura.required'=>'A altura é obrigatória!',
      'altura.min'=>'A altura tem de ter 2 algarismos!',
      'name.required'=>'O Username é obrigatório!',
      'email.required'=>'O email é obrigatório!',
      'password.required'=>'A password é obrigatória!',

    ];

    $this->validate($request,$validacoes,$mensagens); 


    $user = new User();
    $user->name=$request->name;
    $user->email=$request->email;
    $user->password=bcrypt($request->password);
    $user->tipo=$request->tipo;
    $user->save();


    $utilizador = new Utilizador();
    $utilizador->id_user=$user->id;
    $utilizador->nome=$request->nome;
    $utilizador->dataNasc=$request->dataNasc;
    $utilizador->sexo=$request->sexo;
    $utilizador->sns=$request->sns;
    $utilizador->morada=$request->morada;
    $utilizador->peso=$request->peso;
    $utilizador->altura=$request->altura;
    $utilizador->save();

    return view('dados.pessoais.utilizador');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $utente = Utilizador::find($id);
        $user = User::find($utilizador->id_user);
        return view('dados.pessoais.utilizador', compact('utilizador', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $utente = Utente::findOrFail($id);

          return view('editarUtilizador', compact('utilizador'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
