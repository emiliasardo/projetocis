<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\dadosClinicos;
use Illuminate\Support\Facades\Auth;


class DadosClinicosController extends Controller
{
     public function index()
  {
      return view('dadosClinicos');
  }

  public function store(Request $r) {
    $regras = [
        'data' => 'required',
        'doencas' => 'required',
        'peso' => 'required',
        'altura' => 'required',
        'imc' => 'required',
        'registo' => 'required',
        'glicemia' => 'required',
        'insulina' => 'required'
    ];

    $r->validate($regras);

    $dc = new User();
    $dc->name = $r->input('name');
    $dc->email = $r->input('email');
    $dc->password = bcrypt($r->input('password'));
    $dc->save();


    $dc = new dadosClinicos();
    $dc->data = $r->input('data');
    $dc->doencas = $r->input('doencas');
    $dc->peso = $r->input('peso');
    $dc->altura = $r->input('altura');
    $dc->imc = $r->input('imc');
    $dc->registo = $r->input('registo');
    $dc->glicemia = $r->input('glicemia');
    $dc->insulina = $r->input('insulina');
    $dc->user_id = $dc->id;
    $dc->save();

    return ("Dados inseridos com sucesso");
  }

   public function edit(){
$user=Auth::user();



     return view('dadosClinicos');
  }

  public function update(Request $r) {

$regras = [
        'data' => 'required',
        'doencas' => 'required',
        'peso' => 'required',
        'altura' => 'required',
        'imc' => 'required',
        'registo' => 'required',
        'glicemia' => 'required',
        'insulina' => 'required'
    ];

    $r->validate($regras);

      $user=Auth::user();



    $user->dadosClinicos->data = $r->input('data');
    $user->dadosClinicos->doencas = $r->input('doencas');
    $user->dadosClinicos->peso = $r->input('peso');
    $user->dadosClinicos->altura = $r->input('altura');
    $user->dadosClinicos->imc = $r->input('imc');
    $user->dadosClinicos->registo = $r->input('registo');
    $user->dadosClinicos->glicemia = $r->input('glicemia');
    $user->dadosClinicos->insulina = $r->input('insulina');
    $user->dadosClinicos->save();

    return ("Dados Clinicos alterados com sucesso!");
  }

}










