<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profissional;


class ProfissionalController extends Controller
{
  public function index()
  {
      return view('perfilProfissional');
  }

  public function store(Request $r) {
    $regras = [
        'name' => 'required',
        'cedula' => 'required|integer|min:100000000|max:999999999',
        'area' => 'required',
        'email' => 'required|unique:users|email',
        'password' => 'required',
        'password_confirmation' => 'required'


    ];

    $r->validate($regras);

    $p = new User();
    $p->name = $r->input('name');
    $p->email = $r->input('email');
    $p->password = bcrypt($r->input('password'));
    $p->save();

    $prof = new Profissional();
    $prof->cedula = $r->input('cedula');
    $prof->area = $r->input('area');
    $prof->indarea = $r->input('indarea');
    $prof->user_id = $p->id;
    $prof->save();

    return ("Profissional registado com sucesso!");
  }
}







/* class ProfissionalController extends Controller
{
    public function index(){
    
    $profissionais = Profissional::all();
    return view('listaProfissionais', compact('profissionais'));
    
    }


    public function create(){
    return view('novoProfissional');
    }

    public function store(Request $request){

    //vetor de regras
    $regras = [
    'nome' => 'required',
    'dataNasc' => 'required',
    'sexo' => 'required',
    'morada' => 'required',
    ];

    $mensagens = [
    'nome.required' => 'O nome é obrigatório',
    'dataNasc.required' => 'A data de nascimento é obrigatória',
    'sexo.required' => 'O sexo é obrigatório',
    'morada.required'=>'A morada é obrigatória',
    ];

    $this->validate($request,$regras,$mensagens);

    $user = new User;
    $user->name=$request->name;
    $user->email=$request->email;
    $user->password=bcrypt($request->password);
    $user->tipo=$request->tipo;
    $user->save();

    $profissional = new Profissional;
    $profissional->id_user=$user->id;
    $profissional->nome=$request->nome;
    $profissional->dataNasc=$request->dataNasc;
    $profissional->sexo=$request->sexo;
    $profissional->morada=$request->morada;
    $profissional->save();

    return view('dados.pessoais.profissional');
    
    }


    public function show($id){

    $profissional = Profissional::find($id);
    return view('dados.pessoais.profissional', compact('profissional'));
    }


    public function edit($id){

    $profissional = Profissional::findOrFail($id);

    return view('editarProfissional', compact('profissional'));
    }


    public function update($id, Request $request){

    $regras = [
    //par atributo-valor
    'nome' => 'required',
    'dataNasc' => 'required',
    'morada' => 'required',
    ];

    $mensagens = [
    //campo.regra (mas sem o parametro) => 'mensagem de erro'
    'nome.required' => 'O nome é obrigatório',
    'dataNasc.required' => 'A data de nascimento é obrigatória',
    'morada.required'=>'A morada é obrigatória',
    ];

    $this->validate($request,$regras,$mensagens); //caso a validação falhe, volta para o formulário anterior

    //vai ao $request buscar os campos
    //dd($request->all());
    $nome= $request->input('nome');
    $dataNasc=$request->input('dataNasc');
    $sexo=$request->input('sexo');
    $morada=$request->input('morada');

    Profissional::where('id', $id)->update([
        'nome' => $nome,
        'dataNasc' => $dataNasc,
        'sexo' => $sexo,
        'morada' => $morada,
    ]);

    $profissionais = Profissional::all();
    return view('dados.pessoais.profissionais', compact('profissionais'));
    }

}
*/

