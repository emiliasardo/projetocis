<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller{
    //
    public function index(){
        $users = User::all(); //tenho de restringir a apresentar só tipo = admin
        
        return view('listaAdmin', compact('users'));
    }

    public function create(){
        return view('novoAdmin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

      $validacoes = [
      'name' => 'required',
      'email'=> 'required|email',
      'password'=> 'required',
      ];

      $mensagens = [
      'name.required'=>'O Username é obrigatório!',
      'email.required'=>'O email é obrigatório e tem de ser válido!',
      'password.required'=>'A password é obrigatória!',
      ];

    $this->validate($request,$validacoes,$mensagens); 


    $user = new User();
    $user->name=$request->name;
    $user->email=$request->email;
    $user->password=bcrypt($request->password);
    $user->tipo=$request->tipo;
    $user->save();

    return view('listaAdmin');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        
        $admin = User::find($id);
        return view('informacaoAdmin', compact('admin'));
    }

	public function update(Request $request, $id){
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        //
    }
}
