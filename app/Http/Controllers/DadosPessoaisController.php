<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\dadosPessoais;
use Illuminate\Support\Facades\Auth;

class DadosPessoaisController extends Controller
{
     public function index()
  {
      return view('dadosPessoais');
  }

  public function store(Request $r) {
    $regras = [
        'data' => 'required',
        'sexo' => 'required',
        'morada' => 'required'

    ];

    $r->validate($regras);

    $dp = new User();
    $dp->name = $r->input('name');
    $dp->email = $r->input('email');
    $dp->password = bcrypt($r->input('password'));
    $dp->save();


    $dp = new dadosPessoais();
    $dp->name = $r->input('name');
    $dp->data = $r->input('data');
    $dp->sexo = $r->input('sexo');
    $dp->morada = $r->input('morada');
    $dp->user_id = $dp->id;
    $dp->save();

    return ("Utilizador criado com sucesso");
  }

  public function edit(){
$user=Auth::user();



     return view('dadosPessoais');
  }

  public function update(Request $r) {



    $regras = [
        'data' => 'required',
        'sexo' => 'required',
        'morada' => 'required'

    ];

    $r->validate($regras);

    $user=Auth::user();

  
    $user->dadosPessoais->data = $r->input('data');
    $user->dadosPessoais->sexo = $r->input('sexo');
    $user->dadosPessoais->morada = $r->input('morada');
    $user->dadosPessoais->save();

    

    return ("Dados Pessoais alterados com sucesso!");
  }
}

