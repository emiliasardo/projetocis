<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Utente;

class UtenteController extends Controller
{
  public function index()
  {
      return view('perfilUtente');
  }

  public function store(Request $r) {
  	$regras = [
  		'name' => 'required',
      'numero' => 'required|integer|min:100000000|max:999999999',
  		'email' => 'required|unique:users|email',
  		'password' => 'required',
  		'password_confirmation' => 'required'
  	];

  	$r->validate($regras);
  

  	$u = new User();
  	$u->name = $r->input('name');
  	$u->email = $r->input('email');
  	$u->password = bcrypt($r->input('password'));

  	$u->save();

  	$utente = new Utente();
  	$utente->num_utente = $r->input('numero');
  	$utente->user_id = $u->id;
  	$utente->save();

  	return ("Utente registado com sucesso!");
  }
}
