<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Utente extends Model{

    public function dadosClinicos(){
      return $this->hasMany('App\DadoClinico', 'id_utilizador');
    }

    public function user(){
      return $this->belongsTo('App\Users','id_user');
    }

    public function idade(){
      $dateOfBirth=date("Y-m-d", strtotime($this->dataNasc) );
      return $dateOfBirth;
    }
}