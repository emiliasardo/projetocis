<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DadoClinico extends Model
{
    public function utilizador()
    {
    	return $this->belongsTo('App\Utilizador');
    }
}
