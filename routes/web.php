<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('welcome'); } );

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::get('/utente/perfil', 'UtenteController@index')->name('perfilUtente');
Route::get('/profissional/perfil', 'ProfissionalController@index')->name('perfilProf');


/* Diabetes */
Route::get('/Diabetes', function() { return view('Diabetes'); } ) ->name('Diabetes');


/* Eventos */
Route::get('/Eventos', 'EventController@index' ) ->name('Eventos');


/* Links Uteis */
Route::get('/linksUteis', function() { return view('linksUteis'); } ) ->name('linksUteis');


/*Mensagens*/
 Route::get('/mensagens', 'MensagensController@index' ) ->name('mensagens');
 Route::get('/emensagens',function(){return view('emensagens');}) -> name('emensagens');

/* Profissional de Saúde */


/* Profissional de Saúde */
//Route::get('/registoefetuadop', function(){
//	return view('registoefetuadop');
//});



Route::get('/dadosPessoais', 'DadosPessoaisController@edit' )->name('dadosPessoais.create');
Route::post('/dadosp-edit','DadosPessoaisController@update')->name('dadosPessoais.store');


Route::get('/dadosClinicos','DadosClinicosController@edit' )->name('dadosClinicos.create');
Route::post('/dadosc-edit','DadosClinicosController@update')->name('dadosClinicos.store');


/* Contactos */
Route::get('/contactos', function() { return view('contactos'); } )->name('contactos');


/*Tipo de registo*/
Route::get('/selecao-registo', function() { return view('selecao'); } )->name('registo-novo');


/*Registo do Utente*/
Route::get('/registo-utente', function() { return view('registo_utente'); } )->name('utente.create');

Route::post('/registo-utente', 'UtenteController@store')->name('utente.store');


/*Registo do Profissional*/
Route::get('/registo-profissional', function() { return view ('registo_profissional'); } )->name('profissional.create');
Route::post('/registo-profissional','ProfissionalController@store')->name('profissional.store');


/*Editar Dados*/

Route::get('/editarDados', function() { return view('editar_dados'); } );

Route::get('/editarDados', function() { return view('editar_dados'); } );


/*Mensgens Privadas*/
Route::get('/mensagem', function() { return view('mensagem'); } );
Route::get('/nova_mensagem', function() { return view('nova_mensagem'); } );

